# Whats?
This is a simple command line tool for exec docker command on multiple **containers** (only for windows).

# Installation
* Close all terminals
* Add file on you PC as ```C:\\tools\dockerh\dockerh.cmd```
* Add Environnement varible : 
  * right click my computer
  * click Properties
  * click Advanced System Settings
  * click Environment Variables
  * In the top pane (User's variables) find Path, select it and click Edit
  * Add new entry : ```C:\tools\dockerh```

# Usage
* Synthax : ```dockerh [DOCKER-ATTRIBUT] [START-WITH-CONTAINER-NAME]```
* Sample usage on Terminal or powershell context : 
  * start all containers starting with term 'mycon' : ```dockerh start mycon```
  * stop all containers starting with term 'mycon' : ```dockerh stop mycon```
  * read logs (need uniq key) : ```dockerh "logs -f" mycon-uniq-term```
* Limitation : 
  * DockerH work only for docker command with "multiple" container syntax. I.E : ```docker start container1 container2 container3```