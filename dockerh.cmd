@echo off
setlocal

echo %1 containers
docker container ls -a --filter name=%2

echo -----------------
echo running
powershell docker %1 $(docker container ls -aq --filter name=%2)

endlocal